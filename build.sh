clear
echo "Building application..."
go build -o bin/linux/Game

echo "Build Done!"

if [ "$1" == "and-run" ]; then
    echo "Launching Application..."
    ./bin/linux/Game
else
    if [ "$1" == "help" ]; then
        echo "and-run   Builds the application and runs it"
    fi
fi