package main;

import(
	"github.com/go-gl/gl/v4.6-compatibility/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	//"github.com/go-gl/mathgl/mgl64"
	"goGLEngine/DisplayManager"
	"runtime"
	"log"
	"fmt"
);

func init() {
	runtime.LockOSThread();
}

func main() {
	// Initialize DisplayManager and glfw window
	dp := DisplayManager.CreateDisplayManager();
	err := dp.InitWindow(640, 480, "Go GL Test", false);
	if err != nil {
		panic(err);
	}
	defer dp.TerminateWindow();

	fmt.Printf("\n%s\n", dp.GetInfo());
	

	// Create OpenGL Context
	err = gl.Init();
	version := gl.GoStr(gl.GetString(gl.VERSION));
	fmt.Printf("OpenGL Version %s\n", version);

	// Maybe put that into display manager when switching resolution or setting fullscreen...
	//gl.Viewport(0, 0, (int32)(dp.Width), (int32)(dp.Height));
	dp.Window.SetFramebufferSizeCallback(framebufferSizeCallback);

	for !dp.Window.ShouldClose() {

		gl.Clear(gl.COLOR_BUFFER_BIT);
		gl.ClearColor(1.0, 1.0, 1.0, 1.0);

		// Do rendering stuff
		

		if dp.Window.GetKey(glfw.KeyEscape) == glfw.Press {
			dp.Window.SetShouldClose(true);
			log.Printf("Closing Application on expected behaviour\n");
		}

		dp.PostUpdate();
	}


}

func framebufferSizeCallback(w *glfw.Window, width int, height int) {
	log.Printf("Resizing Window to %dx%d\n", width, height);
	gl.Viewport(0, 0, int32(width), int32(height));
}